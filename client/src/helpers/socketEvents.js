import socketIOClient from 'socket.io-client'

const socket = socketIOClient('/');

const socketOn = {
  updateUser: (callback) => {
    socket.on('updateUser', (user) => {
      callback(user)
    })
  },
  updateUsers: (callback) => {
    socket.on('updateUsers', (users) => {
      callback(users)
    })
  },
  updateRoom: (callback) => {
    socket.on('updateRoom', (room) => {
      callback(room)
    })
  },
  updateRooms: (callback) => {
    socket.on('updateRooms', (rooms) => {
      callback(rooms)
    })
  }
}

const socketEmit = {
  joinUser: (userName, callback) => {
    socket.emit('joinUser', userName, err => callback(err))
  },
  createRoom: (roomDetails, callback) => {
    socket.emit('createRoom', roomDetails, err => callback(err))
  },
  joinRoom: (roomDetails, callback) => {
    socket.emit('joinRoom', roomDetails, err => callback(err))
  },
  leaveRoom: (roomName) => {
    socket.emit('leaveRoom', roomName)
  },
  clientMessage: (message, roomName) => {
    socket.emit('clientMessage', { message, roomName })
  },
  locationMessage: ({ latitude, longitude }, roomName, callback) => {
    socket.emit('locationMessage', { latitude, longitude }, roomName, state => callback(state))
  },
  broadcastMessage: (whoWrite) => {
    socket.emit('broadcastMessage', whoWrite)
  }
}

export { socketOn, socketEmit }
