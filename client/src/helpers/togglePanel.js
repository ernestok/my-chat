const openLeftPanel = () => {
  const leftpanel = document.getElementsByClassName('leftpanel')[0]
  if (leftpanel.classList.contains('leftpanel--closed')) {
    leftpanel.classList.remove('leftpanel--closed')
    leftpanel.classList.add('leftpanel--open')
  }
}

const closeLeftPanel = () => {
  const leftpanel = document.getElementsByClassName('leftpanel')[0]
  if (leftpanel.classList.contains('leftpanel--open')) {
    leftpanel.classList.remove('leftpanel--open')
    leftpanel.classList.add('leftpanel--closed')
  }
  return true
}

export { openLeftPanel, closeLeftPanel }
