function scrollToBottom() {
  const messages = document.getElementById('messages')
  if (messages.childNodes.length > 0) {
    const newMessage = messages.lastElementChild

    const clientHeight = messages.clientHeight
    const scrollTop = messages.scrollTop
    const scrollHeight = messages.scrollHeight
    const newMessageHeight = newMessage.clientHeight
    const lastMessageHeight = newMessage.previousElementSibling ? newMessage.previousElementSibling.clientHeight : null
    if (clientHeight + scrollTop + newMessageHeight + lastMessageHeight >= scrollHeight) {
      messages.scrollTop = scrollHeight
    }
  }
}

export { scrollToBottom }
