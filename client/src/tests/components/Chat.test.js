import React from 'react'
import { shallow } from 'enzyme'
import Chat from '../../components/Chat'
import { socketEmit } from '../../helpers/socketEvents'
import { messageChat } from '../fixtures/messageChat'


let wrapper

describe('<Chat /> before login', () => {
  beforeEach(() => {
    socketEmit.clientMessage = jest.fn()
    socketEmit.broadcastMessage = jest.fn()
    wrapper = shallow(<Chat />)
  })

  it('should render before login snapshot', () => {
    expect(wrapper).toMatchSnapshot()
  })
})

describe('<Chat /> default', () => {
  beforeEach(() => {
    wrapper = shallow(<Chat />)
    wrapper.setState({
      user: {
        id: 'newMateUser',
        name: 'Admin'
      }
    });
  })

  it('should render proper Chat snapshot', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('should send to the server and print message back', () => {
    wrapper.find('form').simulate('submit', messageChat)
    expect(socketEmit.clientMessage).toHaveBeenCalledTimes(1)
  })

  it('should broadcast if user click keyboard', () => {
    wrapper.find('input').at(1).simulate('keyUp', messageChat)
    expect(socketEmit.broadcastMessage).toHaveBeenCalledTimes(1)
  })
})
