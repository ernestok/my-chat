import React from 'react'
import { shallow } from 'enzyme'
import LoginPage from '../../components/LoginPage'
import { loginSubmit, loginUserName } from '../fixtures/loginSubmit'
import { socketEmit } from '../../helpers/socketEvents'

let wrapper

describe('<LoginPage />', () => {
  beforeEach(() => {
    socketEmit.joinUser = jest.fn()
    wrapper = shallow(<LoginPage />)
  })

  it('should render LoginPage snapshot', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('should render error if no username is provided', () => {
    wrapper.find('form').simulate('submit', loginSubmit)
    expect(wrapper.state('error')).toBe('enter your username')
  })

  it('should render if username is provided', () => {
    wrapper.find('form').simulate('submit', loginUserName)
    expect(socketEmit.joinUser).toHaveBeenCalledTimes(1)
  })
})