import React from 'react'
import { shallow } from 'enzyme'
import PasswordModal from '../../components/PasswordModal'
import { passwordWrong, passwordCorrect } from '../fixtures/passwordSubmit.js'
import { socketEmit } from '../../helpers/socketEvents'

let wrapper

describe('<PasswordModal />', () => {
  beforeEach(() => {
    socketEmit.joinRoom = jest.fn()
    wrapper = shallow(
      <PasswordModal
        isOpen={true}
        onRequestClose={jest.fn()}
      />
    )
  })

  it('should render PasswordModal snapshot', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('should render error if wrong password is provided', () => {
    wrapper.find('form').simulate('submit', passwordWrong)
    expect(wrapper.state('error')).toBe('Correct password is required')
  })

  it('should pass if any password is provided', () => {
    wrapper.find('form').simulate('submit', passwordCorrect)
    expect(socketEmit.joinRoom).toHaveBeenCalledTimes(1)
  })
})


