import React from 'react'
import { shallow } from 'enzyme'
import MyMessage from '../../components/MyMessage'
import { myMessage } from '../fixtures/messageTypes.js'
import moment from 'moment'

let wrapper

describe('<MyMessage /> with text', () => {
  beforeEach(() => {
    wrapper = shallow(<MyMessage message={myMessage[0]} />)
  })

  it('should render MyMessage snapshot', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('should render props correctly for text message', () => {
    expect(wrapper.find('.message--box').children()).toHaveLength(2)
    expect(wrapper.find('.message--time').text()).toEqual(moment(0).format('H:mm'))
    expect(wrapper.find('.message--text').text()).toEqual(myMessage[0].message)
    expect(wrapper.find('.message.from').hasClass('message--first')).toEqual(true)
  })
})

describe('<MyMessage />  with url', () => {
  beforeEach(() => {
    wrapper = shallow(<MyMessage message={myMessage[1]} />)
  })

  it('should render MyMessage snapshot', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('should render props correctly for url message', () => {
    expect(wrapper.find('.message--box').children()).toHaveLength(2)
    expect(wrapper.find('.message--time').text()).toEqual('')
    expect(wrapper.find('.message.from').hasClass('message--first')).toEqual(true)
  })
})


