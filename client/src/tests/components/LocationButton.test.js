import React from 'react'
import { shallow } from 'enzyme'
import LocationButton from '../../components/LocationButton'
import { position } from '../fixtures/positionGps'

let wrapper

describe('<Chat /> before login', () => {
  beforeEach(() => {
    window.alert = jest.fn();
    wrapper = shallow(<LocationButton />)
  })

  it('should render before login snapshot', () => {
    expect(wrapper).toMatchSnapshot()
    expect(wrapper.state('disabled')).toEqual(false)
  })

  it('should show allert if browser not support geolocaltion', () => {
    wrapper.find('button').simulate('click', position)
    expect(window.alert).toBeCalledWith('Geolocation not supported by your browser.')
  })
})
