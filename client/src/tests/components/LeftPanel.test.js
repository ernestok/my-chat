import React from 'react'
import { shallow } from 'enzyme'
import LeftPanel from '../../components/LeftPanel'
import { user, users } from '../fixtures/user'
import { rooms, roomError, roomCorrect } from '../fixtures/rooms'
import { socketEmit } from '../../helpers/socketEvents'

let wrapper

describe('<LeftPanel /> default', () => {
  beforeEach(() => {
    socketEmit.createRoom = jest.fn()
    wrapper = shallow(
      <LeftPanel
        user={user}
        users={users}
        rooms={rooms}
        openWindowRoom={jest.fn()}
      />
    )
  })

  it('should render LeftPanel snapshot', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('should render props correctly for LeftPanel', () => {
    expect(wrapper.find('.leftpanel--user-name').text()).toEqual(user.name)
    expect(wrapper.find('.leftpanel--users-list').children()).toHaveLength(users.length)
    expect(wrapper.find('.leftpanel--rooms-list').children()).toHaveLength(rooms.length)
  })

  it('should render error if room name has not been written', () => {
    wrapper.find('form').simulate('submit', roomError)
    expect(wrapper.state('error').length).toBeGreaterThan(0)
  })

  it('should call function to create room correctly', () => {
    wrapper.find('form').simulate('submit', roomCorrect)
    expect(socketEmit.createRoom).toHaveBeenCalledTimes(1)
    expect(wrapper.state('error')).toBeNull()
  })
})
