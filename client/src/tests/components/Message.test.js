import React from 'react'
import { shallow } from 'enzyme'
import Message from '../../components/Message'
import { message } from '../fixtures/messageTypes'
import moment from 'moment'

let wrapper

describe('<Message /> with text', () => {
  beforeEach(() => {
    wrapper = shallow(<Message message={message[0]} />)
  })

  it('should render Message snapshot', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('should render props correctly for text Message', () => {
    expect(wrapper.find('.message--box').children()).toHaveLength(2)
    expect(wrapper.find('.message--time').text()).toEqual(moment(0).format('H:mm'))
    expect(wrapper.find('.message--text').text()).toEqual(': ' + message[0].message)
    expect(wrapper.find('.message.to').hasClass('message--first')).toEqual(true)
  })
})

describe('<Message />  with url', () => {
  beforeEach(() => {
    wrapper = shallow(<Message message={message[1]} />)
  })

  it('should render Message snapshot', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('should render props correctly for url Message', () => {
    expect(wrapper.find('.message--box').children()).toHaveLength(2)
    expect(wrapper.find('.message--time').text()).toEqual('')
    expect(wrapper.find('.message.to').hasClass('message--first')).toEqual(true)
  })
})
