import React from 'react'
import { shallow } from 'enzyme'
import AdminMessage from '../../components/AdminMessage'
import { adminMessage } from '../fixtures/messageTypes.js'
import moment from 'moment'

let wrapper

describe('<AdminMessage /> default', () => {
  beforeEach(() => {
    wrapper = shallow(<AdminMessage message={adminMessage[0]} />)
  })

  it('should render AdminMessage snapshot', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('should render props correctly for admin message', () => {
    expect(wrapper.find('.message--monit').children()).toHaveLength(2)
    expect(wrapper.find('.message--time').text()).toEqual('')
    expect(wrapper.find('.message--admin').text()).toEqual(adminMessage[0].message)
  })
})


describe('<AdminMessage /> with showTime to true', () => {
  beforeEach(() => {
    wrapper = shallow(<AdminMessage message={adminMessage[1]} />)
  })

  it('should render time correctly if set showTime to true', () => {
    expect(wrapper.find('.message--time').text()).toEqual(moment(0).format('H:mm'))
  })
})
