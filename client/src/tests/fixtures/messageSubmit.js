const messageSubmit = {
  preventDefault: () => { },
  target: {
    elements: {
      userName: {
        value: ''
      }
    }
  }
}

export {
  messageSubmit
}