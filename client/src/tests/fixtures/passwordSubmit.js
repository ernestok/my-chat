const passwordWrong = {
  preventDefault: () => { },
  target: {
    elements: {
      password: {
        value: ''
      }
    }
  }
}

const passwordCorrect = {
  preventDefault: () => { },
  target: {
    elements: {
      password: {
        value: 'any1'
      }
    }
  }
}

export {
  passwordWrong,
  passwordCorrect
}