const user = {
  id: '1923847',
  name: 'SomeTestedUser'
}

const users = [
  {
    id: '1923848',
    name: 'Poul'
  },
  {
    id: '1923849',
    name: 'John'
  }
]

export {
  user,
  users
} 