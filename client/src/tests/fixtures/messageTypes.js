import moment from 'moment'

const myMessage = [
  {
    author: 'Brzechwa',
    createdAt: moment(0).valueOf(),
    isUrl: false,
    message: 'wiadomosc wlasciwa',
    showTime: true,
  },
  {
    author: 'Brzechwa',
    createdAt: moment(0).valueOf(),
    isUrl: true,
    message: 'www.test.ca',
    showTime: false,
  }
]

const message = [
  {
    author: 'Brzechwa',
    createdAt: moment(0).valueOf(),
    isUrl: false,
    message: 'wiadomosc wlasciwa',
    showTime: true,
  },
  {
    author: 'Brzechwa',
    createdAt: moment(0).valueOf(),
    isUrl: true,
    message: 'www.test.ca',
    showTime: false,
  }
]

const adminMessage = [
  {
    author: {
      name: 'Admin'
    },
    createdAt: moment(0).valueOf(),
    message: 'admins notify test',
    showTime: false,
  },
  {
    author: {
      name: 'Admin'
    },
    createdAt: moment(0).valueOf(),
    message: 'admins notify test',
    showTime: true,
  }
]

export {
  adminMessage,
  message,
  myMessage
}