const rooms = [
  {
    name: 'Main Chat',
    password: null,
    users: ['SomeTestedUser', 'John', 'Poul'],
    messages: []
  },
  {
    name: 'some Chat',
    password: 'testPass',
    users: ['SomeTestedUser', 'John'],
    messages: []
  }
];

const roomError = {
  preventDefault: () => { },
  target: {
    elements: {
      newRoom: {
        value: ''
      },
      password: {
        value: 'pass1'
      }
    }
  }
}

const roomCorrect = {
  preventDefault: () => { },
  target: {
    elements: {
      newRoom: {
        value: 'roomeName1'
      },
      password: {
        value: 'pass1'
      }
    }
  }
}


export {
  rooms,
  roomError,
  roomCorrect
}