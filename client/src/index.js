import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import Chat from './components/Chat'

class App extends Component {
  render () {
    return (
      <Chat/>
    )
  }
}

ReactDOM.render(<App />, document.querySelector('#root'))
