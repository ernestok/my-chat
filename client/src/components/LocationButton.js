import React from 'react'
import { MdLocationOn } from 'react-icons/md'
import PropTypes from 'prop-types'

import { socketOn, socketEmit } from '../helpers/socketEvents'

class LocationButton extends React.Component {
  constructor() {
    super()

    this.state = {
      disabled: false
    }

    socketOn.updateRoom((room) => {
      this.setState({ room: room.name })
    })

    this.sendLocation = this.sendLocation.bind(this)
  }

  sendLocation(e) {
    e.preventDefault()
    if (!navigator.geolocation) {
      return alert('Geolocation not supported by your browser.')
    }
    this.setState({ disabled: true })
    const currentRoom = this.props.room

    navigator.geolocation.getCurrentPosition((position) => {
      socketEmit.locationMessage({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      },
        currentRoom, (state) => {
          this.setState({ disabled: state })
        })
    }, () => {
      this.setState({ disabled: false })
      alert('Unable to fetch location')
    })
  }

  render() {
    var opts = {}
    if (this.state.disabled) {
      opts['disabled'] = 'disabled'
      opts['cover'] = 'Sending..'
    } else {
      opts['cover'] = <MdLocationOn className="location-icon" size="20px" />
    }

    return (

      <button {...opts} type="button" onClick={e => this.sendLocation(e)} className="location--button" title="Send current location">
        {opts['cover']}
      </button>
    )
  }
}

LocationButton.propTypes = {
  room: PropTypes.string
}

LocationButton.defaultProps = {
  room: '',
};

export default LocationButton
