import React from 'react'
import { MdSend, MdMenu } from 'react-icons/md'

import LoginPage from './LoginPage'
import LeftPanel from './LeftPanel'
import LocationButton from './LocationButton'
import Message from './Message'
import MyMessage from './MyMessage'
import AdminMessage from './AdminMessage'
import { socketOn, socketEmit } from '../helpers/socketEvents'
import { scrollToBottom } from '../helpers/scrollToBottom'
import { openLeftPanel } from '../helpers/togglePanel'

class Chat extends React.Component {
  constructor() {
    super()

    this.state = {
      user: null,
      users: [],
      room: null,
      rooms: [],
      disabled: false,
      darkTheme: true,
      prevRoom: 'Main Chat'
    }

    this.broadcastMessage = this.broadcastMessage.bind(this)
    this.openWindowRoom = this.openWindowRoom.bind(this)
    this.changeTheme = this.changeTheme.bind(this)

    socketOn.updateUser((user) => {
      this.setState({ user })
    })

    socketOn.updateUsers((users) => {
      this.setState({ users })
    })

    socketOn.updateRoom((room) => {
      this.setState({ room: room.name })
    })

    socketOn.updateRooms((rooms) => {
      this.setState({ rooms })
    })
  }


  static showLeftPanel() {
    return openLeftPanel()
  }

  componentDidUpdate() {
    const viewExist = document.getElementById('messages')
    if (viewExist) {
      scrollToBottom()
    }
  }

  sendMessage(e) {
    e.preventDefault()

    const text = e.target.elements.text.value.trim()

    if (text) {
      socketEmit.clientMessage(text, this.state.room)
      e.target.elements.text.value = ''
    }
  }

  openWindowRoom(room) {
    this.setState({ room })
  }

  changeTheme() {
    if (!this.state.darkTheme) {
      document.getElementById('theme').href = '/dark.css'
    } else {
      document.getElementById('theme').href = '/light.css'
    }

    this.setState(prevState => ({
      darkTheme: !prevState.darkTheme
    }))
  }

  clearInput() {
    if (this.state.room === this.state.prevRoom) {
      this.setState({ prevRoom: this.state.room })
    }
  }

  broadcastMessage(e) {
    const inputValue = e.target.value
    const whoWrite = {
      user: this.state.user.name,
      room: this.state.room
    }

    if (inputValue) {
      whoWrite.remove = false
      socketEmit.broadcastMessage(whoWrite)
    } else {
      whoWrite.remove = true
      socketEmit.broadcastMessage(whoWrite)
    }
  }

  render() {
    const openedRoom = this.state.rooms.find(room => room.name === this.state.room)
    const whoType = openedRoom ? openedRoom.whoWrite.filter((user) => user !== this.state.user.name) : []

    if (!this.state.user) {
      return <LoginPage />
    }

    return (
      <div className="chat--main">
        <LeftPanel
          user={this.state.user}
          users={this.state.users}
          rooms={this.state.rooms}
          openWindowRoom={this.openWindowRoom}
        />
        <div className="rightpanel">
          <div className="chat--room">
            <div className="menu">
              <button onClick={() => Chat.showLeftPanel()} title="Open Menu">
                <MdMenu size="26px" />
              </button>
            </div>
            <h2 className="chat--roomname">{this.state.room}</h2>
            <div className="toggle toggle--daynight">
              <input onChange={this.changeTheme} type="checkbox" id="toggle--daynight" className="toggle--checkbox" />
              <label className="toggle--btn" htmlFor="toggle--daynight">
                <span className="toggle--feature"></span>
              </label>
            </div>
          </div>
          <div id="messages" className="chat--messages-wrapper">

            {this.state.room && openedRoom.messages.map((message) => {
              if (message.author.name && message.author.name === this.state.user.name) {
                return <MyMessage
                  key={message.createdAt}
                  message={message}
                />
              } else if (message.author.name === 'Admin') {
                return <AdminMessage
                  key={message.createdAt}
                  message={message}
                />
              }
              return <Message
                key={message.createdAt}
                message={message}
              />
            })}
            {this.state.room && whoType.map((whoWrite) => {
              return <p className="message--monit" key={whoWrite}>{whoWrite} is typing..</p>
            })}
          </div>
          <div className="chat--input">
            <LocationButton
              room={this.state.room}
            />
            <form onSubmit={e => this.sendMessage(e)} className="send--form">
              <input onKeyUp={this.broadcastMessage} type="text" name="text" placeholder="Write message..." spellCheck="false" autoFocus autoComplete="off" />
              <button type="submit" className="send--button">
                <MdSend className="send--icon" size="20px" />
              </button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default Chat
