import React from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'

const AdminMessage = ({ message }) => {
  return (
    <div className="message--monit">
      <div className="message--time">{message.showTime ? moment(message.createdAt).format('H:mm') : ''}</div>
      <p className="message--admin">{message.message}</p>
    </div>
  )
}

AdminMessage.propTypes = {
  message: PropTypes.shape({
    author: PropTypes.shape.isRequired,
    createdAt: PropTypes.number.isRequired,
    message: PropTypes.string.isRequired,
    showTime: PropTypes.bool.isRequired,
  }).isRequired,
}

export default AdminMessage
