import React from 'react'
import { MdLockOutline, MdExitToApp, MdControlPoint, MdClose } from 'react-icons/md'
import { FaHashtag } from 'react-icons/fa'
import PropTypes from 'prop-types'

import PasswordModal from './PasswordModal'
import { socketEmit } from '../helpers/socketEvents'
import { closeLeftPanel } from '../helpers/togglePanel'

class LeftPanel extends React.Component {
  constructor() {
    super()

    this.state = {
      error: null,
      passwordModal: {
        open: false,
        roomName: null
      }
    }

    this.createRoomPanel = this.createRoomPanel.bind(this)
    this.joinRoom = this.joinRoom.bind(this)
    this.passwordRoom = this.passwordRoom.bind(this)
  }

  static quitLeftPanel() {
    return closeLeftPanel()
  }

  createRoomPanel(e) {
    e.preventDefault()

    const newRoom = e.target.elements.newRoom.value.trim()
    const password = e.target.elements.password.value.trim()
    const roomDetails = { name: newRoom, password }

    if (!newRoom) {
      return this.setState({ error: 'enter room name' })
    }

    socketEmit.createRoom(roomDetails, (err) => {
      this.setState({ error: err })
    })
    this.setState({ error: null })
    e.target.elements.newRoom.value = ''
    e.target.elements.password.value = ''
  }

  joinRoom(room) {
    const roomDetails = { name: room.name, password: '' }
    socketEmit.joinRoom(roomDetails, () => {
    })
  }

  leaveRoom(roomName) {
    socketEmit.leaveRoom(roomName)
  }

  passwordRoom(roomName) {
    this.setState(prevState => ({
      passwordModal: {
        open: !prevState.passwordModal.open,
        roomName
      }
    }))
  }

  render() {
    return (
      <div className="leftpanel leftpanel--closed">
        <div className="leftpanel--header">
          <h1>Hey <span className="leftpanel--user-name">{this.props.user.name}</span></h1>
          <div className="close--icon">
            <button onClick={() => LeftPanel.quitLeftPanel()} title="Hide Menu">
              <MdClose size="26px" />
            </button>
          </div>
        </div>
        <h2>Users logged:</h2>
        <ol className="leftpanel--users-list">
          {this.props.users.map(user => {
            return (
              <li key={user.id}>
                <FaHashtag size="16px" />
                {user.name}
              </li>
            )
          })}
        </ol>
        <h2>Rooms:</h2>
        <ol className="leftpanel--rooms-list">
          {this.props.rooms.map((room) => {
            if (room.name !== 'Main Chat') {
              return (
                <li className="room--container" key={room.name}>
                  <FaHashtag size="26px" />
                  <div onClick={() => room.users.includes(this.props.user.name) && this.props.openWindowRoom(room.name)} className="room--info">
                    <p>
                      {room.name}
                    </p>
                    <div className="room--user-list">
                      {room.users.join(', ').length > 15 ? `${room.users.join(', ').slice(0, 25)}...` : room.users.join(', ')}
                    </div>
                  </div>
                  {!room.users.includes(this.props.user.name) && !room.password &&
                    <button onClick={() => { this.joinRoom({ name: room.name }); LeftPanel.quitLeftPanel() }} className="room--join" title="Join room">
                      <MdControlPoint size="26px" />
                    </button>}
                  {!room.users.includes(this.props.user.name) && room.password &&
                    <button onClick={() => { this.passwordRoom(room.name); LeftPanel.quitLeftPanel() }} className="room--join" title="Enter password and join room">
                      <MdLockOutline size="26px" />
                    </button>}
                  {room.users.includes(this.props.user.name) &&
                    <button onClick={() => this.leaveRoom(room.name)} className="room--leave" title="Leave room">
                      <MdExitToApp size="26px" />
                    </button>}
                </li>
              )
            } else {
              return (
                <li className="room--container" key={room.name}>
                  <FaHashtag size="26px" />
                  <div onClick={() => room.users.includes(this.props.user.name) && this.props.openWindowRoom(room.name)} className="room--info">
                    <p>{room.name}</p>
                    <div className="room--user-list">
                      {room.users.join(', ').length > 25 ? `${room.users.join(', ').slice(0, 25)}...` : room.users.join(', ')}
                    </div>
                  </div>
                </li>
              )
            }
          }
          )}
        </ol>
        <h2>Add room</h2>
        <form onSubmit={this.createRoomPanel} className="roomCreate--form">
          <div className="roomCreate--container">
            <input type="text" name="newRoom" placeholder="Room name..." maxLength="20" autoComplete="off" />
            <input type="text" name="password" placeholder="Password (optional)" maxLength="20" autoComplete="off" />
          </div>
          <p className="error">{this.state.error}</p>
          <button type="submit" className="button">create room</button>
        </form>
        <PasswordModal
          isOpen={this.state.passwordModal.open}
          onRequestClose={() => this.passwordRoom(this.state.passwordModal.roomName)}
          roomName={this.state.passwordModal.roomName}
        />
      </div>
    )
  }
}


LeftPanel.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string
  }).isRequired,
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
  rooms: PropTypes.arrayOf(PropTypes.object).isRequired,
  openWindowRoom: PropTypes.func.isRequired,
};

export default LeftPanel
