import React from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'

const Message = ({ message }) => {
  if (message.isUrl) {
    return (
      <div className="message--box to">
        <div className="message--time">{message.showTime ? moment(message.createdAt).format('H:mm') : ''}</div>
        <div className={(!message.next ? ('message--first ') : ('')) + 'message to'}>
          <p>{message.author.name}: <a href={message.message} className="message--link" target="_blank" rel="noopener noreferrer">My Location</a></p>
        </div>
      </div>
    )
  }
  return (
    <div className="message--box to">
      <div className="message--time">{message.showTime ? moment(message.createdAt).format('H:mm') : ''}</div>
      <div className={(!message.next ? ('message--first ') : ('')) + 'message to'}>
        <p className="message--text">{message.author.name}: {message.message}</p>
      </div>
    </div>
  )
}

Message.propTypes = {
  message: PropTypes.shape({
    author: PropTypes.shape.isRequired,
    createdAt: PropTypes.number.isRequired,
    isUrl: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired,
    showTime: PropTypes.bool.isRequired,
  }).isRequired,
}

export default Message 
