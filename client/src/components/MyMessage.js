import React from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'

const MyMessage = ({ message }) => {
  if (message.isUrl) {
    return (
      <div className="message--box from">
        <div className="message--time">{message.showTime ? moment(message.createdAt).format('H:mm') : ''}</div>
        <div className={(!message.next ? ('message--first ') : ('')) + 'message from'}>
          <a href={message.message} className="message--link" target="_blank" rel="noopener noreferrer">My Location</a>
        </div>
      </div>
    )
  }
  return (
    <div className="message--box from">
      <div className="message--time">{message.showTime ? moment(message.createdAt).format('H:mm') : ''}</div>
      <div className={(!message.next ? ('message--first ') : ('')) + 'message from'}>
        <p className="message--text">{message.message}</p>
      </div>
    </div>
  )
}

MyMessage.propTypes = {
  message: PropTypes.shape({
    author: PropTypes.shape.isRequired,
    createdAt: PropTypes.number.isRequired,
    isUrl: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired,
    showTime: PropTypes.bool.isRequired,
  }).isRequired,
}

export default MyMessage 
