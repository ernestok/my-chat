import React from 'react'
import { socketEmit } from '../helpers/socketEvents'

class LoginPage extends React.Component {
  constructor() {
    super()

    this.state = {
      error: null,
      endpoint: 'http://localhost:9000' // this is where we are connecting to with sockets
    }

    this.loginUser = this.loginUser.bind(this)
  }

  loginUser(e) {
    e.preventDefault()

    const userName = e.target.elements.userName.value.trim()

    if (!userName) {
      return this.setState({ error: 'enter your username' })
    }

    socketEmit.joinUser(userName, (err) => {
      this.setState({ error: err })
    })

    e.target.elements.userName.value = ''
  }

  render() {
    return (
      <div className="login--page">
        <div className="login--box">
          <form onSubmit={this.loginUser} className="login--form-field">
            <h3>Welcome to Instant Chat</h3>
            <p className="error">{this.state.error}</p>
            <input type="text" name="userName" placeholder="Nick name..." maxLength="20" autoFocus autoComplete="off" />
            <button type="submit" className="button">Join</button>
          </form>
        </div>
      </div>
    )
  }
}

export default LoginPage
