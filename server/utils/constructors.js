const moment = require('moment')

class Users {
  constructor() {
    this.users = [
      {
        id: 'T0j3s74dm!n',
        name: 'Admin'
      }
    ]
  }

  addUser(id, name) {
    if (this.users.filter(user => user.name === name)[0]) {
      return 'Username taken'
    }

    const user = { id, name }
    this.users.push(user)
  }

  getUser(id) {
    return this.users.filter((user) => user.id === id)[0]
  }

  getUsers() {
    return this.users
  }

  removeUser(id) {
    this.users = this.users.filter(user => user.id !== id)
  }
}

class Rooms {
  constructor() {
    this.rooms = [
      {
        name: 'Main Chat',
        users: [],
        messages: [],
        url: [],
        password: null,
        whoWrite: []
      }
    ]
  }

  getRooms() {
    return this.rooms
  }

  getRoom(roomName) {
    return this.rooms.find(room => room.name === roomName)
  }

  addRoom(roomName, password) {
    if (this.rooms.filter(room => room.name === roomName)[0]) {
      return 'Room name taken'
    }
    this.rooms.push({
      name: roomName,
      users: [],
      messages: [],
      url: [],
      password,
      whoWrite: []
    })
  }

  addUser(userName, roomName) {
    const room = this.rooms.find(room => room.name === roomName)
    if (!room.users.find(user => user === userName)) {
      room.users.push(userName)
    }
  }
  addWhoWrite(whoWrite, roomName) {
    const room = this.rooms.find(room => room.name === roomName)
    if (!room.whoWrite.find(user => user === whoWrite)) {
      room.whoWrite.push(whoWrite)
    }
    const that = this
    setTimeout(() => { that.removeWhoWrite(whoWrite, roomName) }, 5000)
  }

  removeUser(userName, roomName) {
    const room = this.getRoom(roomName)
    room.users = room.users.filter(user => user !== userName)

    if (!room.users.length) {
      this.removeRoom(roomName)
    }
  }

  removeWhoWrite(whoWrite, roomName) {
    const room = this.getRoom(roomName)
    room.whoWrite = room.whoWrite.filter(user => user !== whoWrite)
  }

  removeRoom(roomName) {
    this.rooms = this.rooms.filter(room => {
      if (roomName !== 'Main Chat') {
        return room.name !== roomName
      }
      return room
    })
  }

  addMessage(message, roomName) {
    const room = this.rooms.find(room => room.name === roomName)
    if (room.messages.length >= 40) {
      room.messages.shift()
    }
    room.messages.push(message)
  }

  adminMessage(userName, action, roomName) {
    const notify = userName + ' ' + action + '!'
    const message = {
      author: {
        name: 'Admin'
      },
      createdAt: moment().valueOf(),
      message: notify,
      showTime: false
    }
    this.checkShowTime(roomName, message)
    return message
  }

  continuousMessages(roomName, message) {
    const room = this.getRoom(roomName)
    if (room.messages.length && message.author === room.messages[room.messages.length - 1].author) {
      message.next = true
    }
  }

  checkShowTime(roomName, message) {
    const room = this.getRoom(roomName)
    if (room.messages.length && message.createdAt - room.messages[room.messages.length - 1].createdAt > 60000) {
      message.showTime = true
    } else if (room.messages.length === 0) {
      message.showTime = true
    }
  }
}

module.exports = { Rooms, Users }
