const generateLocationMessage = (latitude, longitude) => {
  return `https://mail.google.com/maps?q=${latitude},${longitude}`
}

module.exports = { generateLocationMessage }
