const path = require('path')
const http = require('http')
const express = require('express')
const socketIO = require('socket.io')
const moment = require('moment')

const app = express()
const server = http.createServer(app)
const io = socketIO(server)
const publicPath = path.join(__dirname, './../client/public', 'dist')
const port = process.env.PORT || 9000

const { Users, Rooms } = require('./utils/constructors')
const { generateLocationMessage } = require('./utils/geolocation')

const users = new Users()
const rooms = new Rooms()

app.use(express.static(publicPath))

io.on('connection', (socket) => {
  socket.on('joinUser', (userName, callback) => {
    const err = users.addUser(socket.id, userName)

    if (err) {
      return callback(err)
    }
    rooms.addUser(userName, 'Main Chat')
    rooms.addMessage(rooms.adminMessage(userName, 'joined', 'Main Chat'), 'Main Chat')

    socket.join('Main Chat')

    io.emit('updateUsers', users.getUsers())
    socket.emit('updateUser', users.getUser(socket.id))
    io.emit('updateRooms', rooms.getRooms())
    socket.emit('updateRoom', rooms.getRoom('Main Chat'))
  })

  socket.on('createRoom', (roomDetails, callback) => {
    const err = rooms.addRoom(roomDetails.name, roomDetails.password)
    if (err) {
      return callback(err)
    }

    const userName = users.getUser(socket.id).name
    rooms.addUser(userName, roomDetails.name)
    rooms.addMessage(rooms.adminMessage(userName, ' joined room', roomDetails.name), roomDetails.name)

    io.emit('updateRooms', rooms.getRooms())
    socket.emit('updateRoom', rooms.getRoom(roomDetails.name))
  })

  socket.on('joinRoom', (roomDetails, callback) => {
    const room = rooms.getRoom(roomDetails.name)

    if (room && room.password !== roomDetails.password) {
      callback('Wrong password')
    } else {
      const userName = users.getUser(socket.id).name
      rooms.addUser(userName, roomDetails.name)
      rooms.addMessage(rooms.adminMessage(userName, ' joined room', roomDetails.name), roomDetails.name)

      io.emit('updateRooms', rooms.getRooms())
      socket.emit('updateRoom', rooms.getRoom(roomDetails.name))
      callback(null)
    }
  })

  socket.on('leaveRoom', (roomName) => {
    const userName = users.getUser(socket.id).name
    rooms.addMessage(rooms.adminMessage(userName, ' has left room', roomName), roomName)
    rooms.removeUser(userName, roomName)

    socket.emit('updateRoom', rooms.getRoom('Main Chat'))
    io.emit('updateRooms', rooms.getRooms())
  })

  socket.on('disconnect', () => {
    const user = users.getUser(socket.id)
    if (user) {
      rooms.addMessage(rooms.adminMessage(user.name, ' has left', 'Main Chat'), 'Main Chat')
      users.removeUser(user.id)
      rooms.removeUser(user.name, 'Main Chat')

      socket.emit('updateRoom', rooms.getRoom('Main Chat'))
      io.emit('updateUsers', users.getUsers())
    }

    socket.leave('Main Chat')
    io.emit('updateRooms', rooms.getRooms())
  })

  socket.on('clientMessage', (data) => {
    const message = {
      author: users.getUser(socket.id),
      isUrl: false,
      message: data.message,
      createdAt: moment().valueOf(),
      showTime: false
    }

    rooms.continuousMessages(data.roomName, message)
    rooms.checkShowTime(data.roomName, message)
    rooms.addMessage(message, data.roomName)

    io.emit('updateRooms', rooms.getRooms())
  })

  socket.on('locationMessage', (coords, roomName, callback) => {
    const url = generateLocationMessage(coords.latitude, coords.longitude)
    const message = {
      author: users.getUser(socket.id),
      isUrl: true,
      message: url,
      createdAt: moment().valueOf(),
      showTime: false
    }

    rooms.continuousMessages(roomName, message)
    rooms.checkShowTime(roomName, message)
    if (url) {
      rooms.addMessage(message, roomName)

      io.emit('updateRooms', rooms.getRooms())
      callback(false)
    }
  })

  socket.on('broadcastMessage', (whoWrite) => {
    if (!whoWrite.remove) {
      rooms.addWhoWrite(whoWrite.user, whoWrite.room)
    } else {
      rooms.removeWhoWrite(whoWrite.user, whoWrite.room)
    }
    io.emit('updateRooms', rooms.getRooms())
  })
})

server.listen(port, () => {
  console.log('\x1b[36m%s\x1b[0m', `Started on port ${port}`)
})
