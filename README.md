# Read Me #
![dark mode](https://bitbucket.org/ernestok/my-chat/raw/e22506bb9abbd80a0dd245d10f12cc604fe4274d/githubImg/chat-dark.png)
![lightmode](https://bitbucket.org/ernestok/my-chat/raw/e22506bb9abbd80a0dd245d10f12cc604fe4274d/githubImg/chat-light.png)

## Description
Chat application for writing messeges and sending location between people. Works on desktop, and remote devices.

Demo version can be seen <a href="https://chatapp-12.herokuapp.com/">here</a>.

## Features: ##

    1. One public chat for all logged users.
    2. Possibility for creating chat rooms.
    3. Rooms can have option for setting password when are created.
    4. Users can be in many rooms simultaneously.
    5. Send your geolocation.
    6. Change color theme.
    7. Follow the latest messages in window room, if already scrolled to the bottom. 
    8. Show the time of latest message, if last one were more than 60 seconds ago.
    9. Clearing each chat window from the oldest messages, if there are more messages than 40.

## Author information ##

Ernest Kost

ernest.kost@gmail.com



## Installation and launching ##


### 1. Installing npm packages ###

  In order to launch the app, firstly you need to install the required npm packages for server-side and client-side.
  For server-side change your directory to folder with 
  `server.js` and `package.json` then use the command:
  
  `npm install`

  Right just after use commands for client-side:

  `cd client`
  `npm install`
  
### 2. Launching the app in production environment ###

  In order to see website in developer environment on local machine go to main folder (where the server.js is) and use command:
  
  `npm run dev:watch`
  
  
###  Used tools and frameworks ###

  
  - [React](https://reactjs.org)
  - [Express](https://expressjs.com)
  - [Socket.io](https://socket.io)
  - [Webpack](https://webpack.js.org)
  - [SCSS](http://sass-lang.com/)
  - [Moment.js](https://momentjs.com)

